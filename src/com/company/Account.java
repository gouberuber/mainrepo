package com.company;

public abstract class Account implements Detailable {

    private Currency currency;
    private double balance;
    private String name;

    public Currency getCurrency() {
        return currency;
    }

    public void setCurrency(Currency currency) {
        this.currency = currency;
    }

    public Account(double balance, String name){
        this.balance = balance;
        this.name = name;
    }
    public Account(double balance, String name, Currency currency){
        this.balance = balance;
        this.name = name;
        this.currency = currency;
    }

    public double getBalance() {
        return balance;
    }

    public String getName() {
        return name;
    }


    public void setBalance(double balance) {
        this.balance = balance;
    }

    public void setName(String name) {
        this.name = name;
    }

    public abstract void addInterest();
    public String getDetails(){
        return "Account: " + this.name + ", Balance: " + this.balance;
    }

}
