package com.company;

import java.time.*;
import java.time.format.DateTimeFormatter;
import java.time.format.FormatStyle;
import java.util.Calendar;
import java.util.Set;
import java.util.TimeZone;

public class PlayingWithDates {
    public static void main(String[] args) {
        LocalDateTime dateNow = LocalDateTime.now();
        LocalDateTime birthday = LocalDateTime.of (Year.now().getValue() + 1, 1, 28,0,0);
        Duration duration = Duration.between(dateNow, birthday);
        long diff = Math.abs(duration.toDays());
        System.out.println("Days until my birthday: " + diff);

        //America/New_York
        ZoneId zoneId = ZoneId.of("America/New_York");
        ZonedDateTime zd = ZonedDateTime.now();
        ZonedDateTime ny_date_time = zd.withZoneSameInstant(zoneId);
        System.out.println("Time in NYC: " + ny_date_time.format(DateTimeFormatter.ofLocalizedTime(FormatStyle.FULL)));
        LocalDateTime finishAt = LocalDateTime.of(dateNow.getYear(), dateNow.getMonth(), dateNow.getDayOfMonth(), 17, 0);
        System.out.println("Training will finish in " + Duration.between(dateNow,finishAt).toMinutes() + " minutes");
        System.out.println("My birthday on " + birthday.format(DateTimeFormatter.ofLocalizedDate(FormatStyle.MEDIUM)) + " will be a " + birthday.getDayOfWeek());

        //Need to find next Friday 13th



    }
}
