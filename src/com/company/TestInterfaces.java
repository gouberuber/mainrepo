package com.company;

public class TestInterfaces {

    public static void main(String[] args) {
        Detailable[] d = new Detailable[3];
        d[0]  = new CurrentAccount(100.10, "Vlad");
        d[1] = new SavingsAccount(200.50, "Vlad-Savings");
        d[2] = new HomeInsurance(1000, 500, 5000);
        for(Detailable in : d){
            System.out.println(in.getDetails());
        }

    }
}
