package com.company;
import com.sun.source.tree.Tree;

import java.util.HashSet;
import java.util.Iterator;
import java.util.TreeSet;
import java.util.Set;


public class CollectionsTest {
    public static void main(String[] args) {
        Set<Account> accounts = new TreeSet<>((Account o1, Account o2) -> {
            return Double.compare(o2.getBalance(), o1.getBalance());
        });
        CurrentAccount ca  = new CurrentAccount(500.2, "Vlad");
        SavingsAccount sa = new SavingsAccount(21.50, "Vlad-Savings");
        SavingsAccount sa_two = new SavingsAccount(2.2121, "Vlad-Savings");
        accounts.add(ca);
        accounts.add(sa);
        accounts.add(sa_two);
        Iterator<Account> it = accounts.iterator();
        while(it.hasNext()){
            Account current = it.next();
            System.out.println(current.getBalance());
            System.out.println(current.getName());
            current.addInterest();
            System.out.println(current.getBalance());
            System.out.println();
        }

        for(Account a : accounts){
            System.out.println(a.getBalance());
            System.out.println(a.getName());
            a.addInterest();
            System.out.println(a.getBalance());
            System.out.println();
        }
        accounts.forEach(a -> {
            System.out.println(a.getBalance());
            System.out.println(a.getName());
            a.addInterest();
            System.out.println(a.getBalance());
            System.out.println();
        });
    }
}
