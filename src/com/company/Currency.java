package com.company;

public enum Currency {
    GBP('£'), EUR('€'), USD('$');
    private char symbol;

    public char getSymbol() {
        return symbol;
    }

    private Currency(char c){
        this.symbol = c;
    }
}
