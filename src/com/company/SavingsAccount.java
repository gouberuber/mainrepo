package com.company;

public class SavingsAccount extends Account {

    public SavingsAccount(double balance,String name){
        super(balance, name);
    }
    public SavingsAccount(double balance, String name, Currency c){
        super(balance, name, c);
    }

    @Override
    public void addInterest() {
        setBalance(getBalance() * 1.4);
    }
}
