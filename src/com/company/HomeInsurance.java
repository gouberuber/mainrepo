package com.company;

public class HomeInsurance implements Detailable{

    private int premiums, excess, amount;

    public HomeInsurance(int premiums, int excess, int amount){
        this.premiums = premiums;
        this.excess = excess;
        this.amount = amount;
    }

    @Override
    public String getDetails() {
        return "" + this.premiums + " " + this.excess;
    }
}
