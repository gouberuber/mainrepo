package com.company;

public class Main {

    public static void main(String[] args) {
	// write your code here
        String make = "Renault";
        String model = "Laguna";
        double engineSize = 1.8;
        byte gear = 2;
        System.out.println("The make is " + make);
        System.out.println("The model is" + model);
        System.out.println("The engine size is " + engineSize);
        if(engineSize <= 1.3){
            System.out.println("Weak car");
        }
        else{
            System.out.println("Powerful car");
        }
        if(gear == 1){
            System.out.println("Below 10mph");
        }else if(gear == 2){
            System.out.println("Above 10mph but below 20mph");
        }else if(gear == 3){
            System.out.println("Above 20mph, but below 40mph");
        }

        int count = 1;
        for(int i = 1900; i <= 2000; i++){
            if(i%4 == 0){
                System.out.println(i);
                count++;
            }
            if(count == 6) break;
        }



    }
}
